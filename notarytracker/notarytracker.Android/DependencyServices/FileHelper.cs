﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using notarytracker.Infrastructure.DataAccess;
using Xamarin.Forms;
using notarytracker.Droid.DependencyServices;
using System.IO;

[assembly: Dependency(typeof(FileHelper))]
namespace notarytracker.Droid.DependencyServices
{
    public class FileHelper : IFileHelper
    {
        public string GetLocalFilePath(string filename)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, filename);

        }
    }
}