﻿using notarytracker.Model;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notarytracker.Data
{
    public class NotaryRepository
    {
        private static List<NotaryDetail> _source = new List<NotaryDetail>
            {
                new NotaryDetail
                {
                    SignerName="Jim Jones",
                    InstrumentType="Title",
                    DateSigned=new DateTime(2017,10,10)
                },
                new NotaryDetail
                {
                    SignerName="Kelly Pride",
                    InstrumentType="Mortgage",
                    DateSigned=new DateTime(2017,10,08)
                },
                new NotaryDetail
                {
                    SignerName="Princess Buttercup",
                    InstrumentType="Marriage License",
                    DateSigned=new DateTime(2017,10,9)
                },
            };
        private readonly notarytrackerDatabase _database;

        public NotaryRepository(notarytrackerDatabase database)
        {
            _database = database;
        }

        public async Task<IEnumerable<NotarySummary>> GetSummaries()
        {
            var items = await _database.GetItemsAsync();
            return items.Select(d => new NotarySummary
            {
                SignerName = d.SignerName,
                InstrumentType = d.InstrumentType,
                DateSigned = d.DateSigned
            });

            //return _source.Select(d=> new NotarySummary
            //{
            //    SignerName = d.SignerName,
            //    InstrumentType = d.InstrumentType,
            //    DateSigned = d.DateSigned
            //});
        }

        public async void AddNewNotary(NotaryDetail notary)
        {
            await _database.SaveItemAsync(notary);
            //_source.Add(notary);
        }
    }

    public class notarytrackerDatabase
    {
        static SQLiteAsyncConnection database;

        public notarytrackerDatabase(string dbPath)
        {
            database = new SQLiteAsyncConnection(dbPath);
            database.CreateTableAsync<NotaryDetail>().Wait();
        }

        public Task<List<NotaryDetail>> GetItemsAsync()
        {
            return database.Table<NotaryDetail>().ToListAsync();
        }

        public Task<NotaryDetail> GetItemAsync(int id)
        {
            return database.Table<NotaryDetail>().Where(d => d.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(NotaryDetail detail)
        {
            if(detail.ID != 0)
            {
                return database.UpdateAsync(detail);
            }
            else
            {
                return database.InsertAsync(detail);
            }
        }

        public Task<int> DeleteItemAsync(NotaryDetail detail)
        {
            return database.DeleteAsync(detail);
        }
    }
}
