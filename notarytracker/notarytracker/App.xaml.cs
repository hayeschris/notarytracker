﻿using GalaSoft.MvvmLight.Ioc;
using GalaSoft.MvvmLight.Views;
using notarytracker.Data;
using notarytracker.Infrastructure;
using notarytracker.Infrastructure.DataAccess;
using notarytracker.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace notarytracker
{
    public partial class App : Application
    {
        static notarytrackerDatabase database;
        public static notarytrackerDatabase Database => database ?? (database = new notarytrackerDatabase(DependencyService.Get<IFileHelper>().GetLocalFilePath("notarytrackerDatabase.db3")));

        private static ViewModelLocator _locator;

        public static ViewModelLocator Locator
        {
            get
            {
                return _locator ?? (_locator = new ViewModelLocator());
            }
        }

        public static Page GetMainPage()
        {
            return new NavigationPage(new MainPage());
        }

        public App()
        {
            InitializeComponent();
            var nav = new NavigationService();
            nav.Configure(ViewModelLocator.MainPage, typeof(MainPage));
            nav.Configure(ViewModelLocator.NewPage, typeof(NewPage));
            nav.Configure(ViewModelLocator.DetailsPage, typeof(DetailsPage));
            SimpleIoc.Default.Register<INavigationService>(() => nav);
            SimpleIoc.Default.Register<NotaryRepository>(() => new NotaryRepository(Database));
            var firstPage = new NavigationPage(new MainPage());

            nav.Initialize(firstPage);

            MainPage = firstPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
