﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notarytracker.ViewModel
{
    public interface IAppearable
    {
        void ViewAppeared();
    }
    public class MyAppViewModelBase : ViewModelBase, IAppearable
    {
        public virtual void ViewAppeared()
        {
        }
    }
}
