﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using notarytracker.Data;
using notarytracker.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notarytracker.ViewModel
{
    public class NewNotaryViewModel : MyAppViewModelBase
    {
        private string _signerName;
        public string SignersName
        {
            get { return _signerName; }
            set
            {
                if (value != _signerName)
                {
                    _signerName = value;
                    Set(() => SignersName, ref _signerName, value);
                }
            }
        }
        private string _instrumentType;
        public string InstrumentType
        {
            get { return _instrumentType; }
            set
            {
                if (value != _instrumentType)
                {
                    _instrumentType = value;
                    Set(() => InstrumentType, ref _instrumentType, value);
                }
            }
        }
        private DateTime _dateSigned = DateTime.Now;
        public DateTime DateSigned
        {
            get { return _dateSigned; }
            set
            {
                if (value != _dateSigned)
                {
                    _dateSigned = value;
                    Set(() => DateSigned, ref _dateSigned, value);
                }
            }
        }
        private double? _fee ;
        public double? Fee
        {
            get { return _fee; }
            set
            {
                if (value != _fee)
                {
                    _fee = value;
                    Set(() => Fee, ref _fee, value);
                }
            }
        }
        private string _notaryType;
        public string NotaryType
        {
            get { return _notaryType; }
            set
            {
                if (value != _notaryType)
                {
                    _notaryType = value;
                    Set(() => NotaryType, ref _notaryType, value);
                }
            }
        }
        private IList<string> _notaryTypeItems = new List<string>
        {
            "Acknowledgment",
            "Jurat",
            "Other"
        };
        public IList<string> NotaryTypeItems => _notaryTypeItems;
        private IList<string> _identificationTypeItems = new List<string>
        {
            "Identification",
            "Personally Known",
            "Witness"
        };
        public IList<string> IdentificationTypeItems => _identificationTypeItems;
        private string _identificationType;
        public string IdentificationType
        {
            get { return _identificationType; }
            set
            {
                if (value != _identificationType)
                {
                    _identificationType = value;
                    Set(() => IdentificationType, ref _identificationType, value);
                    IdentificationRequiresValue = value == "Witness";
                }
            }
        }
        private bool _identificationRequiresValue;
        public bool IdentificationRequiresValue { get { return _identificationRequiresValue; } set {
                Set(() => IdentificationRequiresValue, ref _identificationRequiresValue, value);
            } }
        private string _witnessName;
        public string WitnessName
        {
            get { return _witnessName; }
            set
            {
                if (value != _witnessName)
                {
                    _witnessName = value;
                    Set(() => WitnessName, ref _witnessName, value);
                }
            }
        }

        public NewNotaryViewModel(NotaryRepository repository, INavigationService notaryService)
        {
            _repository = repository;
            _navigationService = notaryService;
        }
        private RelayCommand _add;
        private NotaryRepository _repository;
        private INavigationService _navigationService;

        public RelayCommand Add
        {
            get
            {
                return _add ?? (_add = new RelayCommand(() =>
                {
                    _repository.AddNewNotary(new NotaryDetail
                    {
                        SignerName = SignersName,
                        InstrumentType = InstrumentType,
                        DateSigned = DateSigned
                    });
                    _navigationService.GoBack();
                }));
            }
        }
    }
}
