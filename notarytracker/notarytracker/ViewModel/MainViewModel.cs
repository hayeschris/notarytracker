﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Views;
using notarytracker.Data;
using notarytracker.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notarytracker.ViewModel
{
    public class MainViewModel : MyAppViewModelBase
    {
        public ObservableCollection<NotarySummary> NotarySummaries { get; private set; }

        public MainViewModel(NotaryRepository repository, INavigationService notaryService)
        {
            NotarySummaries = new ObservableCollection<NotarySummary>();
            _repository = repository;
            ResetNotarySummaries();
            _notaryService = notaryService;
        }

        private async void ResetNotarySummaries()
        {
            NotarySummaries.Clear();
            var items = await _repository.GetSummaries();
            foreach (var s in items
                .OrderByDescending(x => x.DateSigned))
                NotarySummaries.Add(s);
        }

        private NotaryRepository _repository;

        private RelayCommand _addNewCommand;
        private INavigationService _notaryService;

        public RelayCommand AddNewCommand
        {
            get
            {
                return _addNewCommand
                    ?? (_addNewCommand = new RelayCommand(
                    () =>
                    {
                        _notaryService.NavigateTo(ViewModelLocator.NewPage);
                    }));
            }
        }

        public INavigationService NotaryService { get; }

        public override void ViewAppeared()
        {
            ResetNotarySummaries();
        }
    }
}
