﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace notarytracker.Model
{
    public class NotarySummary
    {
        public string SignerName { get; set; }
        public string InstrumentType { get; set; }
        public DateTime DateSigned { get; set; }
    }

    public class NotaryDetail
    {
        public int ID { get; set; }
        public string SignerName { get; set; }
        public string InstrumentType { get; set; }
        public DateTime DateSigned { get; set; }
        public double Fee { get; set; }
        public string NotaryType { get; set; }
        public string IdentificationType { get; set; }
        public string AdditionalInformation { get; set; }
    }
}
