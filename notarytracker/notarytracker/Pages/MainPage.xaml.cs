﻿using notarytracker.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace notarytracker
{
    public partial class MainPage : ContentPage
    {
        private object PageData;
        public MainPage()
        {
            InitializeComponent();
            PageData = App.Locator.Main;
            BindingContext = PageData;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((IAppearable)PageData).ViewAppeared();
        }

    }
}
