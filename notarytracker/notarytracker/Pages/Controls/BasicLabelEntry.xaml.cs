﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace notarytracker.Pages.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BasicLabelEntry : ContentView
    {
        public BasicLabelEntry()
        {
            InitializeComponent();
        }

        public static readonly BindableProperty LabelTextProperty = BindableProperty.Create(nameof(LabelText), typeof(string), typeof(BasicLabelEntry));

        public string LabelText
        {
            get
            {
                return (string)GetValue(LabelTextProperty);
            }
            set
            {
                SetValue(LabelTextProperty, value);
            }
        }

        public static readonly BindableProperty PlaceholderTextProperty = BindableProperty.Create(nameof(PlaceholderText), typeof(string), typeof(BasicLabelEntry));

        public string PlaceholderText
        {
            get
            {
                return (string)GetValue(PlaceholderTextProperty);
            }
            set
            {
                SetValue(PlaceholderTextProperty, value);
            }
        }

        public static readonly BindableProperty EntryTextProperty = BindableProperty.Create(nameof(EntryText), typeof(string), typeof(BasicLabelEntry));

        public string EntryText
        {
            get
            {
                return (string)GetValue(EntryTextProperty);
            }
            set
            {
                SetValue(EntryTextProperty, value);
            }
        }
    }
}