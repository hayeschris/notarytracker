﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace notarytracker.Pages.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DateLabelPicker : ContentView
    {
        public DateLabelPicker()
        {
            InitializeComponent();
        }
        public static readonly BindableProperty LabelTextProperty = BindableProperty.Create(nameof(LabelText), typeof(string), typeof(DateLabelPicker));

        public string LabelText
        {
            get
            {
                return (string)GetValue(LabelTextProperty);
            }
            set
            {
                SetValue(LabelTextProperty, value);
            }
        }

        public static readonly BindableProperty DateEntryProperty = BindableProperty.Create(nameof(DateEntry), typeof(DateTime?), typeof(DateLabelPicker));

        public DateTime DateEntry
        {
            get
            {
                if (GetValue(DateEntryProperty) == null) return DateTime.Now;

                return (DateTime)GetValue(DateEntryProperty);
            }
            set
            {
                SetValue(DateEntryProperty, value);
            }
        }


    }
}