﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace notarytracker.Infrastructure.DataAccess
{
    public interface IFileHelper
    {
        string GetLocalFilePath(string filename);
    }

    
}
